package com.herokuapp.theinternet.base;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

public class BaseTest {
	
	protected WebDriver driver;
	
	@BeforeMethod(alwaysRun = true)
	@Parameters({"browser", "environment", "platform", "platformVersion"})
	protected void setUp(@Optional("chrome") String browser, @Optional("local") String environment, @Optional String platform, @Optional String platformVersion, ITestContext ctx ) {
		BrowserDriverFactory factory = new BrowserDriverFactory(browser);
		
		String testName = ctx.getCurrentXmlTest().getName(); 
		
		//Starting test locally or on the grid depending on the environment parameter
		if(environment.equals("grid")) {
			driver = factory.createDriverGrid();
		} else if (environment.equals("stack")){
			driver = factory.createDriverStack(platform, platformVersion, testName);
		} else {
			driver = factory.createDriver();
		}
		
		sleep();
		
		// maximize browser window
		//driver.manage().window().maximize();
		
	}
	
	@AfterMethod(alwaysRun = true)
	protected void tearDown() {
		//Closing driver
		System.out.println("[Closing driver]");
		driver.quit();
	}
	
	protected void sleep() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	protected void takeScreenshot(String fileName) {
		File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String path = System.getProperty("user.dir") + "/test-output/screenshots/" + fileName + ".png";
		System.out.println("The path to save screenshots is: " + path);
		try {
			FileUtils.copyFile(srcFile, new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
