package com.herokuapp.theinternet.base;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class BrowserDriverFactory {
	private ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
	private String browser;
	
	public BrowserDriverFactory(String browser) {
		this.browser = browser.toLowerCase();
	}
	
	public WebDriver createDriver() {
		System.out.println("[Setting up driver: " + browser + "]");
		
		//Creating driver
		switch (browser) {
		case "chrome":
			System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
			driver.set(new ChromeDriver());
			break;
		
		case "firefox":
			System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
			driver.set(new FirefoxDriver());
 			break;
			
		case "safari":
			System.setProperty("webdriver.safari.driver", "src/main/resources/safaridriver");
			driver.set(new SafariDriver());
			break;
			
		default:
			System.out.println("[Couldn't set: " + browser + ". It's unkown. Starting Chrome instead]");
			System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
			driver.set(new ChromeDriver());
			break;
		}
		return driver.get();
	}
	
	//Start test using Selenium Grid
	public WebDriver createDriverGrid() {
		
		URL url = null;
		
		try {
			url = new URL("http://10.150.64.25:4444/wd/hub");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		System.out.println("Starting " + browser + " on grid");
		
		//Using DesiredCapabilities to set up browser
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setBrowserName(browser);
		
		//Creating driver
		try {
			driver.set(new RemoteWebDriver(url, capabilities));
		} catch (WebDriverException e) {
			if (e.getMessage().contains("Error forwarding")) {
				System.out.println("[Couldn't set: " + browser + ". Its unknown. Starting chrome instead]");
				capabilities.setBrowserName("chrome");
				driver.set(new RemoteWebDriver(url, capabilities));
			}
		}
		
		return driver.get();
	}
	
	//Start test using BrowserStack grid
	public WebDriver createDriverStack(String platform, String platformVersion, String testName) {
		String USERNAME = "jesuslomeli1";
		String AUTOMATE_KEY = "UTh1pzLRvwXdgWUMxt2H";
		String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

		System.out.println("Starting " + browser + " on BrowserStack");
		
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("browserName", browser);
		
		if (platform == null) {
			// If platform is not provided, start tests on OS X
			caps.setCapability("os", "OS X");
		} else {
			caps.setCapability("os", platform);
		}
		
		if (platformVersion == null) {
			// If platform version is not provided, start tests on High Sierra
			caps.setCapability("os_version", "High Sierra");
		} else {
			caps.setCapability("os_version", platformVersion);
		}

		// Setting up test name
		caps.setCapability("name", testName);
		
		try {
			driver.set(new RemoteWebDriver(new URL(URL), caps));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		return driver.get();
	}
}
