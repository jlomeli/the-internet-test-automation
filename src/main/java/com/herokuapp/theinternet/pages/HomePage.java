package com.herokuapp.theinternet.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.herokuapp.theinternet.base.BasePage;

public class HomePage extends BasePage{
	
	private String homePageUrl = "http://the-internet.herokuapp.com/";
	
	private By forgotPasswordLinkLocator = By.linkText("Forgot Password");
	private By formAuthenticationLinkLocator = By.linkText("Form Authentication");
	private By javascriptAlertsLinkLocator = By.linkText("JavaScript Alerts");

	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	public void open() {
		openUrl(homePageUrl);
	}
	
	public ForgotPasswordPage clickForgotPasswordLink() {
		find(forgotPasswordLinkLocator).click();
		return new ForgotPasswordPage(driver);
	}
	
	public LoginPage clickformAuthenticationLink() {
		find(formAuthenticationLinkLocator).click();;
		return new LoginPage(driver);
	}
	
	public JavascriptAlertsPage clickJavascriptAlertsLink() {
		find(javascriptAlertsLinkLocator).click();
		return new JavascriptAlertsPage(driver);
	}

}
