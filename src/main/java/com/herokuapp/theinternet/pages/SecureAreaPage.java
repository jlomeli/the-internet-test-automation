package com.herokuapp.theinternet.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.herokuapp.theinternet.base.BasePage;

public class SecureAreaPage extends BasePage{
	
	private By messageLocator = By.id("flash-messages");
	
	public SecureAreaPage (WebDriver driver) {
		super(driver);
	}
	
	public void waitForSecureAreaPageToLoad() {
		waitForVisibilityOf(messageLocator, 3);
	}
	
	public String getMessageText() {
		return find(messageLocator).getText();
	}

}
