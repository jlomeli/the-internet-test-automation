package com.herokuapp.theinternet.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.herokuapp.theinternet.base.BasePage;

public class EmailSentPage extends BasePage{
	
	private By retrieveMessageLocator = By.id("content");
	
	public EmailSentPage(WebDriver driver) {
		super(driver);
	}
	
	public void waitForEmailSentPageToLoad() {
		waitForVisibilityOf(retrieveMessageLocator, 3);
		System.out.println(driver.getCurrentUrl());
	}
	
	public String getMessageText() {
		return find(retrieveMessageLocator).getText();
	}
}
