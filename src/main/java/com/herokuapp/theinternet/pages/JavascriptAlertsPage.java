package com.herokuapp.theinternet.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.herokuapp.theinternet.base.BasePage;

public class JavascriptAlertsPage extends BasePage{
	
	private String javascriptAlertsPageUrl = "http://the-internet.herokuapp.com/javascript_alerts";
	
	private By jsAlertButtonLocator = By.xpath("//button[@onclick='jsAlert()']");
	private By jsConfirmButtonLocator = By.xpath("//button[@onclick='jsConfirm()']");
	private By jsPromptButtonLocator = By.xpath("//button[@onclick='jsPrompt()']");
	private By resultMessageLocator = By.id("result");
	
	private Alert alert;
	
	public JavascriptAlertsPage(WebDriver driver) {
		super(driver);
	}
	
	public void open() {
		openUrl(javascriptAlertsPageUrl);
	}
	
	public void waitForJavascriptAlertsPageToLoad() {
		waitForVisibilityOf(jsAlertButtonLocator, 3);
		System.out.println("Javascript Alerts page opened!");
	}
	
	public void displaySimpleAlert() {
		clickJsAlertButton();
		waitForAlertMessage();
		alert = driver.switchTo().alert();
		System.out.println("Switching to Alert");
		
	}
	
	public void displayConfirmAlert() {
		clickJsConfirmButtonn();
		waitForAlertMessage();
		alert = driver.switchTo().alert();		
	}
	
	public void displayPromptAlert() {
		clickJsPromptButtonn();
		waitForAlertMessage();
		alert = driver.switchTo().alert();		
	}
	
	public String getAlertMessageText() {
		return alert.getText();
	}
	
	public void enterTextOnPromptAlert(String text) {
		alert.sendKeys(text);
	}
	
	public void acceptAlert() {
		alert.accept();
		waitForResultMessage();
	}
	
	public void dissmisAlert() {
		alert.dismiss();
		waitForResultMessage();
	}
	
	public String getResultText() {
		return find(resultMessageLocator).getText();
	}
	
	private void clickJsAlertButton() {
		find(jsAlertButtonLocator).click();	
	}
	
	private void clickJsConfirmButtonn() {
		find(jsConfirmButtonLocator).click();
	}
	
	private void clickJsPromptButtonn() {
		find(jsPromptButtonLocator).click();
	}
	
	private void waitForAlertMessage() {
		waitForVisibillityOfAlert(3);
	}
	
	private void waitForResultMessage() {
		waitForVisibilityOf(resultMessageLocator, 3);
	}
}
