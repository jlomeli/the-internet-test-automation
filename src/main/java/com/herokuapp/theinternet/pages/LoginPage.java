package com.herokuapp.theinternet.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.herokuapp.theinternet.base.BasePage;

public class LoginPage extends BasePage{
	
	private String loginPageUrl = "http://the-internet.herokuapp.com/login";
	
	private By usernameFieldLocator = By.id("username");
	private By passwordFieldLocator = By.id("password");
	private By loginButtonLocator = By.xpath("//button[@type='submit']");
	private By errorMessageLocator = By.id("flash");
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	public void open() {
		openUrl(loginPageUrl);
	}
	
	public void waitForLoginPageToLoad() {
		waitForVisibilityOf(usernameFieldLocator, 3);
	}
	
	public SecureAreaPage positiveLogin(String username, String password) {
		enterCredentials(username, password);
		clickLoginButton();
		return new SecureAreaPage(driver);
	}
	
	public void negativeLogin(String username, String password) {
		enterCredentials(username, password);
		clickLoginButton();
		waitForErrorMessage();
	}

	private void waitForErrorMessage() {
		// TODO Auto-generated method stub
		waitForVisibilityOf(errorMessageLocator, 3);
		
	}
	
	public String getErrorMessageText() {
		return find(errorMessageLocator).getText();
	}

	private void clickLoginButton() {
		// TODO Auto-generated method stub
		System.out.println("Clicking Login button");
		find(loginButtonLocator).click();
	}

	private void enterCredentials(String username, String password) {
		// TODO Auto-generated method stub
		System.out.println("Entering credentials");
		find(usernameFieldLocator).sendKeys(username);
		find(passwordFieldLocator).sendKeys(password);
		
	}
}
