package com.herokuapp.theinternet.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.herokuapp.theinternet.base.BasePage;

public class ForgotPasswordPage extends BasePage{
	
	private String forgotPasswordPageUrl = "http://the-internet.herokuapp.com/forgot_password";
	
	private By emailFieldLocator = By.id("email");
	private By retrievePasswordButtonLocator = By.xpath("//button[@id='form_submit']");
	
	public ForgotPasswordPage (WebDriver driver) {
		super(driver);
	}
	
	public void open() {
		openUrl(forgotPasswordPageUrl);
	}
	
	public void waitForForgotPasswordPageToLoad() {
		waitForVisibilityOf(emailFieldLocator, 3);
	}
	
	public EmailSentPage retrieveForgottenPassword(String email) {
		enterRetrieverEmail(email);
		clickRetrievePasswordButton();
		return new EmailSentPage(driver);
	}
	
	private void enterRetrieverEmail(String email) {
		find(emailFieldLocator).sendKeys(email);
	}
	
	private void clickRetrievePasswordButton() {
		find(retrievePasswordButtonLocator).click();
	}
}
