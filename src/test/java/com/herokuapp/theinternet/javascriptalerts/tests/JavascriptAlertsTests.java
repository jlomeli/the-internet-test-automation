package com.herokuapp.theinternet.javascriptalerts.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.herokuapp.theinternet.base.BaseTest;
import com.herokuapp.theinternet.pages.HomePage;
import com.herokuapp.theinternet.pages.JavascriptAlertsPage;

public class JavascriptAlertsTests extends BaseTest{
	
	@Test(priority = 1)
	public void simpleAlertTest() {
		HomePage homePage = new HomePage(driver);
		homePage.open();
		
		JavascriptAlertsPage javascriptAlertsPage = homePage.clickJavascriptAlertsLink();
		javascriptAlertsPage.waitForJavascriptAlertsPageToLoad();
		
		javascriptAlertsPage.displaySimpleAlert();
		
		Assert.assertTrue(javascriptAlertsPage.getAlertMessageText().equals("I am a JS Alert"), "Alert text is not the expected");
		
		javascriptAlertsPage.acceptAlert();
		
		Assert.assertTrue(javascriptAlertsPage.getResultText().equals("You successfuly clicked an alert"), "Result text is not the expected");
	}
	
	@Test(priority = 2)
	public void confirmAlertTest() {
		HomePage homePage = new HomePage(driver);
		homePage.open();
		
		JavascriptAlertsPage javascriptAlertsPage = homePage.clickJavascriptAlertsLink();
		javascriptAlertsPage.waitForJavascriptAlertsPageToLoad();
		
		javascriptAlertsPage.displayConfirmAlert();
		
		Assert.assertTrue(javascriptAlertsPage.getAlertMessageText().equals("I am a JS Confirm"), "Alert text is not the expected");
		
		javascriptAlertsPage.acceptAlert();
		
		Assert.assertTrue(javascriptAlertsPage.getResultText().equals("You clicked: Ok"), "Result text is not the expected");
	}
	
	@Test(priority = 3)
	public void dismissAlertTest() {
		HomePage homePage = new HomePage(driver);
		homePage.open();
		
		JavascriptAlertsPage javascriptAlertsPage = homePage.clickJavascriptAlertsLink();
		javascriptAlertsPage.waitForJavascriptAlertsPageToLoad();
		
		javascriptAlertsPage.displayConfirmAlert();
		
		Assert.assertTrue(javascriptAlertsPage.getAlertMessageText().equals("I am a JS Confirm"), "Alert text is not the expected");
		
		javascriptAlertsPage.dissmisAlert();
		
		Assert.assertTrue(javascriptAlertsPage.getResultText().equals("You clicked: Cancel"), "Result text is not the expected");
	}
	
	@Test(priority = 4)
	public void promptAlertTest() {
		HomePage homePage = new HomePage(driver);
		homePage.open();
		
		JavascriptAlertsPage javascriptAlertsPage = homePage.clickJavascriptAlertsLink();
		javascriptAlertsPage.waitForJavascriptAlertsPageToLoad();
		
		javascriptAlertsPage.displayPromptAlert();
		
		Assert.assertTrue(javascriptAlertsPage.getAlertMessageText().equals("I am a JS prompt"), "Alert text is not the expected");
		
		javascriptAlertsPage.enterTextOnPromptAlert("This is a test!");
		javascriptAlertsPage.acceptAlert();
		
		Assert.assertTrue(javascriptAlertsPage.getResultText().equals("You entered: This is a test!"), "Result text is not the expected");
	}
}
