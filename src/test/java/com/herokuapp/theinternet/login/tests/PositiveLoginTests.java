package com.herokuapp.theinternet.login.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.herokuapp.theinternet.base.BaseTest;
import com.herokuapp.theinternet.pages.HomePage;
import com.herokuapp.theinternet.pages.LoginPage;
import com.herokuapp.theinternet.pages.SecureAreaPage;

public class PositiveLoginTests extends BaseTest{
	
	@Test(priority = 1)
	public void loginTest() {
		HomePage homePage = new HomePage(driver);
		homePage.open();
		
		//Open Login page
		LoginPage loginPage = homePage.clickformAuthenticationLink();
		
		//Wait for login page to load
		loginPage.waitForLoginPageToLoad();
		
		//Login page locators
		SecureAreaPage secureAreaPage = loginPage.positiveLogin("tomsmith", "SuperSecretPassword!");
		
		//Wait for confirmation page
		secureAreaPage.waitForSecureAreaPageToLoad();
		
		Assert.assertTrue(driver.getCurrentUrl().equals("http://the-internet.herokuapp.com/secure"), "URL is not the expected URL");
		
		
		Assert.assertTrue(secureAreaPage.getMessageText().contains("You logged into a secure area!"), "Message doesn't contain expected text");
	}
	
}
