package com.herokuapp.theinternet.login.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.herokuapp.theinternet.base.BaseTest;
import com.herokuapp.theinternet.pages.LoginPage;

public class NegativeLoginTests extends BaseTest{
	
	@Test(priority = 1)
	public void loginWithWrongUsernameTest() {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		
		//Execute Negative Login
		loginPage.negativeLogin("wrongusername", "SuperSecretPassword!");
		
		Assert.assertTrue(loginPage.getErrorMessageText().contains("Your username is invalid!"), "Message doesn't contain expected text");
		
	}
	
	@Test(priority = 2)
	public void loginWithWrongPasswordTest() {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		
		//Execute Negative Login
		loginPage.negativeLogin("tomsmith", "WrongSuperSecretPassword");
		
		Assert.assertTrue(loginPage.getErrorMessageText().contains("Your password is invalid!"), "Message doesn't contain expected text");
	}

}
