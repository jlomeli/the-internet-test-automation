package com.herokuapp.theinternet.forgotpassword.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.herokuapp.theinternet.base.BaseTest;
import com.herokuapp.theinternet.pages.EmailSentPage;
import com.herokuapp.theinternet.pages.ForgotPasswordPage;
import com.herokuapp.theinternet.pages.HomePage;

public class ForgotPasswordTests extends BaseTest {
	
	@Test(priority = 1)
	public void retrieveForgotPasswordTest() {
		HomePage homePage = new HomePage(driver);
		homePage.open();
		
		//Open Login page
		ForgotPasswordPage forgotPasswordPage = homePage.clickForgotPasswordLink();
		
		//Wait for login page to load
		forgotPasswordPage.waitForForgotPasswordPageToLoad();
		
		EmailSentPage emailSentPage = forgotPasswordPage.retrieveForgottenPassword("test@test.com");
		
		//wait for confirmation page to load
		emailSentPage.waitForEmailSentPageToLoad();
		
		
		Assert.assertTrue(driver.getCurrentUrl().equals("http://the-internet.herokuapp.com/email_sent"), "URL is not the expected URL");
		
		
		Assert.assertTrue(emailSentPage.getMessageText().contains("Your e-mail's been sent!"), "Message doesn't contain expected text");
	}
}
